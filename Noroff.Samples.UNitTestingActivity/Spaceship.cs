﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Noroff.Samples.UnitTestingActivity
{
    public enum Status { Ready, Launched, Landed, Error }

    public class Spaceship
    {
        public Status CurrentStatus { get; private set; } = Status.Ready;
        public double FuelLevel { get; private set; }

        public Spaceship(double initialFuel)
        {
            FuelLevel = initialFuel;
        }

        public void Launch()
        {
            if (FuelLevel < 50)
            {
                CurrentStatus = Status.Error;
                throw new InvalidOperationException("Not enough fuel to launch.");
            }

            FuelLevel -= 50;
            CurrentStatus = Status.Launched;
        }

        public void Land()
        {
            if (CurrentStatus != Status.Launched)
            {
                CurrentStatus = Status.Error;
                throw new InvalidOperationException("Spaceship is not in flight.");
            }

            FuelLevel -= 30;
            CurrentStatus = Status.Landed;
        }

        public void Refuel(double amount)
        {
            FuelLevel += amount;
            // Fuel level should not exceed 100
            if (FuelLevel > 100)
            {
                FuelLevel = 100;
            }
        }

        public string SetDestination(string destination)
        {
            return $"Destination set to {destination}";
        }

        public bool CheckSystems()
        {
            // System check logic - just returns true for simplicity
            return true;
        }
    }
}
