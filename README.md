# Spaceship Control Project

This repository contains a simulation of spacecraft functionalities demonstrated through a .NET class library and unit testing with xUnit.

## Structure

- The main library contains the implementation of a `Spaceship` class with various methods to simulate spacecraft operations.
- The test project is an xUnit Test project containing tests for the `Spaceship` class.

## Branches

- **main**: The starting point of the project, including initial setups and placeholders for the `Spaceship` class and unit tests.
- **solution**: The branch containing the complete solution with a fully implemented `Spaceship` class and corresponding unit tests.

## Getting Started

1. **Clone the Repository:**
   Clone this repository to your local machine to start exploring the project.

   ```
   git clone <repository-url>
   ```

2. **Navigate to the Starting Point:**
   Switch to the `main` branch to start from the beginning.

   ```
   git checkout main
   ```

3. **Explore the Solution:**
   To view the complete solution, switch to the `solution` branch.

   ```
   git checkout solution
   ```

## Working with the Project

- Open the solution in Visual Studio or any preferred .NET IDE.
- Explore the `Spaceship` class in the main library.
- Examine the unit tests in the test project.
- Run the tests to verify the functionality of the `Spaceship` class.

## Contributions

- Feel free to contribute to the project by enhancing the existing code or adding new test cases.
- Follow standard Git workflows for branching and submitting pull requests.

## License

- This project is open source and available under the [MIT License](LICENSE).
